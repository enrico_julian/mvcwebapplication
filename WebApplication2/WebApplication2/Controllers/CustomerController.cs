﻿
using static DataLibrary1.Logic.CustomerProcessor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;
using System.Data.Entity;
using System.Net;


namespace WebApplication2.Controllers
{
    public class CustomerController : Controller
    {
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult List()
        {

            ViewBag.Message = "Customer List";

            var data = LoadCustomer();
            
            List<CustomerModel> customer = new List<CustomerModel>();
            
            foreach (var row in data)
            {
                
                if (row.CustomerStatus == "Active") 
                    {
                        customer.Add(new CustomerModel
                        {
                            Id = row.Id,
                            CustomerName = row.CustomerName,
                            CustomerAddress = row.CustomerAddress,
                            CustomerPhone = row.CustomerPhone,
                            CustomerStatus = row.CustomerStatus
                        });
                    }
            }

            return View(customer);
        }

        public ActionResult Create()
        {
            ViewBag.Message = "Customer registraion";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CustomerModel Model)
        {
            if (ModelState.IsValid)
            {
                int recordsCreated = CreateCustomer(Model.CustomerName,
                    Model.CustomerAddress,
                    Model.CustomerPhone);
                return RedirectToAction("List");
            }

            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Message = "Update Detail";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, CustomerModel Model)
        {
            if (ModelState.IsValid)
            {

                int recordsCreated = EditCustomer(id, Model.CustomerName,
                    Model.CustomerAddress,
                    Model.CustomerPhone);
                return RedirectToAction("List");
            }

            return View();

        }




        public ActionResult Delete(int id)
        {
            int recordDeleted = DeleteCustomer(id);
            return RedirectToAction("List");
        }

    }
}