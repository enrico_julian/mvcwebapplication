﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class CustomerModel
    {

        public int Id { get; set; }

        [Display(Name = "Customer Name")]
        [Required(ErrorMessage = "Enter your name")]
        public string CustomerName { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Enter your address")]
        public string CustomerAddress { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = "Enter your phone number")]
        public string CustomerPhone { get; set; }

        [Display(Name = "Status")]
        public string CustomerStatus { get; set; }

    }

    public class MVCWebApplicationDB : DbContext
    {
        public DbSet<CustomerModel> Customers { get; set; }
    }
}