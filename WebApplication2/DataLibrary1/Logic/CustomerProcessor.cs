﻿using DataLibrary1.DataAccess;
using DataLibrary1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataLibrary1.Logic
{
    public static class CustomerProcessor
    {
        public static int CreateCustomer(string customerName, string customerAddress, string customerPhone)
        {
            CustomerModel data = new CustomerModel
            {
                CustomerName = customerName,
                CustomerAddress = customerAddress,
                CustomerPhone = customerPhone,
                CustomerStatus = "Active"
            };

            string sql = @"insert into dbo.Customer (CustomerName, CustomerAddress, CustomerPhone, CustomerStatus)
                            values (@CustomerName, @CustomerAddress, @CustomerPhone, @CustomerStatus);";
            return SqlDataAccess.SaveData(sql, data);
        }

        public static int EditCustomer(int id, string customerName, string customerAddress, string customerPhone)
        {
            string sql = @"update dbo.Customer set CustomerName = " + "'" + customerName + "'" + ", CustomerAddress = " + "'" + customerAddress + "'" +
                ", CustomerPhone = " + "'" + customerPhone + "'" +
                " where Id=" + id;
            return SqlDataAccess.ExecuteSql(sql);
        }

        public static int DeleteCustomer(int id)
        {
            string sql = @"update dbo.Customer set CustomerStatus = 'InActive' where Id=" + id;
            return SqlDataAccess.ExecuteSql(sql);
        }

        public static List<CustomerModel> LoadCustomer()
        {
            string sql = @"Select Id, CustomerName, CustomerAddress, CustomerPhone, CustomerStatus from dbo.Customer;";
            return SqlDataAccess.LoadData<CustomerModel>(sql);
        }
    }
}
